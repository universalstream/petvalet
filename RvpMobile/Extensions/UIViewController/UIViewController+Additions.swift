//
//  UIViewController+Additions.swift
//  OnDemandApp
//  Created by Pawan Joshi on 12/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

import UIKit
import Presentr

public var presenter: Presentr = {
    let width = ModalSize.full
    let height = ModalSize.fluid(percentage: 1)
    let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 0))
    let customType = PresentationType.custom(width: width, height: height, center: center)
    
    let customPresenter = Presentr(presentationType: customType)
    customPresenter.transitionType = .coverVerticalFromTop
    customPresenter.dismissTransitionType = .coverVerticalFromTop
    customPresenter.roundCorners = false
    customPresenter.backgroundColor = .darkGray
    customPresenter.backgroundOpacity = 0.5
    customPresenter.dismissOnSwipe = true
    customPresenter.dismissOnSwipeDirection = .top
    return customPresenter
}()

// MARK: - UIViewController Extension
extension UIViewController {
    
    /**
     Shows a simple back button
     
     - parameter title:   title for alerview
     - parameter message: message to be shown
     */
    func setBackButton(){
        let btnHome = UIImage(named: "Order_Back_White")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: btnHome, style: .plain, target: self, action:  #selector(barButtonDidTap(_:)))
        
//        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage.init(named: "Order_Back_White"), for: UIControlState.normal)
//        button.addTarget(self, action:#selector((popView:)), for:.touchUpInside)
//        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
//        let rightBarButton : UIBarButtonItem = UIBarButtonItem.init(customView: button)
//        navigationItem.leftBarButtonItem = rightBarButton
        
    }
    @objc func barButtonDidTap(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     Shows a simple alert view with a title and dismiss button
     
     - parameter title:   title for alerview
     - parameter message: message to be shown
     */
    func showAlertViewWithMessage(_ title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    /**
     Shows a simple alert banner with a title and dismiss button
     
     - parameter title:   title for alerview
     - parameter message: message to be shown
 
    func showAlertBannerWith(_ title: String, message: String, bannerStyle:ALAlertBannerStyle, timeDurationInSeconds:TimeInterval) {
        
        let bannerAlertPosition = ALAlertBannerPositionBottom
        let banner = ALAlertBanner(for: self.view, style: bannerStyle, position: bannerAlertPosition, title: title, subtitle: message)
        banner?.secondsToShow = timeDurationInSeconds
        self.view.makeToast(message, duration: 2.0, position: .bottom, title: title, image: nil, style: nil, completion: nil)
        //        self.view.makeToast(message, duration: 1.5, position: ToastPosition.bottom)
    }
    */
    /**
     Shows a simple alert banner with auto hide
     
     - parameter message:     message description
     - parameter bannerStyle: bannerStyle description
 
    func showAlertBannerWithMessage(_ message: String, bannerStyle: ALAlertBannerStyle) {
        
        let bannerAlertPosition = ALAlertBannerPositionTop
        let banner = ALAlertBanner(for: view, style: bannerStyle, position: bannerAlertPosition, title: message)
        banner?.show()
    }
    func showBRYXBanner(_ title: String, _ message: String, _ imageUrl: String?) {
        let banner = Banner(title: title, subtitle: message, image: UIImage(named: "notification-Icon"), backgroundColor: UIColor.black.withAlphaComponent(0.8))
        banner.dismissesOnTap = true
        banner.titleLabel.font = UIFont.init(name: "FSJoey-Bold", size: 15)
        banner.show(duration: 3.0)
    }*/
    /**
     Shows a simple alert view with a title, dismiss button and action handler for dismiss button
     
     - parameter title:         title for alerview
     - parameter message:       message description
     - parameter actionHandler: actionHandler code/closer/block
     */
    func showAlertViewWithMessageAndActionHandler(_ title: String, message: String, actionHandler:(() -> Void)?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default) { (action) in
            
            if let _ = actionHandler {
                actionHandler!()
            }
        }
        alertController.addAction(alAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func alertWithOkAndCancel(_ title: String, message: String, actionHandler:(() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alAction = UIAlertAction(title: "SIGN UP", style: .default) { (action) in
            
            if let _ = actionHandler {
                actionHandler!()
            }
        }
        
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
        }
        
        alertController.addAction(alAction)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    /**
     Shows a simple alert view with a simple text field
     
     - parameter title:                title description
     - parameter message:              message description
     - parameter textFieldPlaceholder: placeholder text for text field in alert view
     - parameter submitActionHandler:  submitActionHandler block/closer/code
     */
    func showAlertViewWithTextField(_ title: String, message: String, textFieldPlaceholder:String, submitActionHandler:@escaping (_ textFromTextField:String?) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = textFieldPlaceholder
            textField.borderStyle = UITextBorderStyle.none
        }
        
        let submitAction = UIAlertAction(title: NSLocalizedString("Submit", comment: "Submit"), style: .default) {  (action: UIAlertAction!) in
            let answerTF = alertController.textFields![0]
            let text = answerTF.text
            submitActionHandler(text)
        }
        alertController.addAction(submitAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) {  (action: UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    /**
     Shows a simple alert view with a secure text field
     
     - parameter title:                title description
     - parameter message:              message description
     - parameter textFieldPlaceholder: placeholder text for text field in alert view
     - parameter submitActionHandler:  submitActionHandler block/closer/code
     */
    func showAlertViewWithSecureTextField(_ title: String, message: String, textFieldPlaceholder:String, submitActionHandler:@escaping (_ textFromTextField:String) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = textFieldPlaceholder
            textField.isSecureTextEntry = true
        }
        
        let submitAction = UIAlertAction(title: NSLocalizedString("Submit", comment: "Submit"), style: .default) {  (action: UIAlertAction!) in
            let answer = alertController.textFields![0]
            submitActionHandler(answer.text!)
        }
        alertController.addAction(submitAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) {  (action: UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension UIViewController {
    /**
     shows activity controller with supplied items. at least one type of item must be supplied
     
     - parameter image:      image to be shared
     - parameter text:       text to be shared
     - parameter url:        url to be shared
     - parameter activities: array of UIActivity which you want to show in controller. nil value will show every available active by default
     */
    func showActivityController(_ image: UIImage?, text: String?, url: String?, activities: [UIActivity]? = nil ) {
        
        var array = [AnyObject]()
        
        if image != nil {
            array.append(image!)
        }
        if text != nil {
            array.append(text! as AnyObject)
        }
        if url != nil {
            array.append(URL(string: url!)! as AnyObject)
        }
        assert(array.count != 0, "Please specify at least element to share among image, text or url")
        
        let activityController = UIActivityViewController(activityItems: array, applicationActivities: activities)
        present(activityController, animated: true, completion: nil)
    }
}
